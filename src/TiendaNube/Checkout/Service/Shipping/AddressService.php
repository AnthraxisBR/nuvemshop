<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Service\Shipping;

use phpDocumentor\Reflection\Types\Mixed_;
use Psr\Log\LoggerInterface;
use TiendaNube\Checkout\Service\Store\StoreService;
use TiendaNube\Checkout\Exception\CepClientServiceException;
use TiendaNube\Checkout\Exception\StoreException;
use TiendaNube\Checkout\Service\Client\CepClientService;

/**
 * Class AddressService
 *
 * @package TiendaNube\Checkout\Service\Shipping
 */
class AddressService
{
    /**
     * The database connection link
     *
     * @var \PDO
     */
    private $store;

    /**
     * The Logger instance
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The CEP finder client instance
     * @var CepClientService
     */
    private $cepClientService;

    /**
     * The Address data
     * @var array
     */
    private $address = [];
    /**
     * AddressService constructor.
     *
     * @param \PDO $pdo
     * @param LoggerInterface $logger
     */
    public function __construct(StoreService $store, LoggerInterface $logger, CepClientService $cepClientService)
    {
        $this->cepClientService = $cepClientService;
        $this->store = $store;
        $this->logger = $logger;
    }

    /**
     * Get an address by its zipcode (CEP)
     *
     * The expected return format is an array like:
     * [
     *      'altitude'=>'7.0',
     *      'cep'=>'40010000',
     *      'latitude'=>'-12.967192',
     *      'longitude'=>'-38.5101976',
     *      'address'=>'Avenida da França',
     *      'neighborhood'=>'Comércio',
     *      'city'=>[
     *          'ddd'=>71,
     *          'ibge'=>'2927408',
     *          'name'=>'Salvador'
     *      ],
     *      'state'=>[
     *          'acronym'=>'BA'
     *      ]
     * ];
     *
     * or false when not found.
     *
     * @param string $zip
     * @return bool|mixed
     */
    public function getAddressByZip(string $zip)
    {

        $this->logger->debug('Getting address for the zipcode [' . $zip . '] from database');

        $this->address = $this->getAddressFromDatabase($zip);

        if($this->address){

            $this->address = $this->getAddressZipData($zip);
            return $this->address;
        }

        return false;
    }

    /**
     * Get full address data from external API (CepAberto http://www.cepaberto.com/api_key )
     *
     * Will return all data from the external API, or false if an error occurred or nothing has founded
     *
     * @param string $zip
     * @return array|mixed
     */
    private function getAddressZipData(string $zip){
        $this->cepClientService->setCepParameter($zip);

        try {
            $zipData = $this->cepClientService->get();

            if($zipData !== false){
                //API Limit per second
                sleep(3);
                $data = json_decode($zipData->getBody()->getContents());
                if (!is_null($data ) && json_last_error() === JSON_ERROR_NONE) {
                    return (array) $this->formatAddressZipDataResponse($data);
                }
            }
            return false;
        }catch (CepClientServiceException $ex){

            $this->logger->error(
                'An error occurred at try to fetch the zip code data from the internet, exception with message was caught: ' .
                $ex->getMessage()
            );

            return false;
        }

    }

    /**
     * Get an address by its zipcode (CEP) from Database
     *
     * The expected return is boolean of search status, the result is store on $this->store->model object
     * @param string $zip
     * @return bool
     */
    private function getAddressFromDatabase(string $zip){

        try {

            $this->store->getCurrentStore()->findZipCode($zip);

            // returning if the address exists
            return $this->store->isBetaTester();

        } catch (StoreException $ex) {
            $this->logger->error(
                'An error occurred at try to fetch the address from the database, exception with message was caught: ' .
                $ex->getMessage()
            );
            return false;
        }
    }

    /**
     * @param mixed $data
     * @return array
     */
    public function formatAddressZipDataResponse($data){
        $new = [];
        $new['altitude'] = number_format($data->altitude,1,'.','');
        $new['cep'] = $data->cep;
        $new['latitude'] = $data->latitude;
        $new['longitude'] = $data->longitude;
        $new['address'] = $data->logradouro;
        $new['neighborhood'] = $data->bairro;
        $new['city'] = [];
        $new['city']['ddd'] = $data->cidade->ddd;
        $new['city']['ibge'] = $data->cidade->ibge;
        $new['city']['name'] = $data->cidade->nome;
        $new['state'] = [];
        $new['state']['acronym'] = $data->estado->sigla;
        return $new;
    }

}
