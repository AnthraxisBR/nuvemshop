<?php

namespace TiendaNube\Checkout\Service\Shipping;

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use TiendaNube\Checkout\Service\Client\CepClientService;
use TiendaNube\Checkout\Service\Store\StoreService;

class AddressServiceTest extends TestCase
{

    public function testGetExistentAddressByZipcode()
    {
        // mocking logger
        $logger = $this->createMock(LoggerInterface::class);

        $address = [
            'altitude' => '7.0',
            'cep' => '40010000',
            'latitude' => '-12.967192',
            'longitude' => '-38.5101976',
            'address' => 'Avenida da França',
            'neighborhood' => 'Comércio',
            'city' => [
                'ddd' => 71,
                'ibge' => '2927408',
                'name' => 'Salvador'
            ],
            'state' => [
                'acronym' => 'BA'
            ]
        ];
        $storeInstance = $this->getStoreServiceInstance(true,$address);


        $CepClientService = $this->getCepClientServiceInstance(true,'40010000');
        // creating service
        $service = new AddressService($storeInstance,$logger,$CepClientService);

        // testing
        $result = $service->getAddressByZip('40010000');

        // asserts
        $this->assertNotNull($result);
        $this->assertEquals($address,$result);
    }


    public function testGetExistentAddressByZipcodeInDatabase()
    {
        // mocking logger
        $logger = $this->createMock(LoggerInterface::class);

        $address = [
            'altitude' => '7.0',
            'cep' => '40010000',
            'latitude' => '-12.967192',
            'longitude' => '-38.5101976',
            'address' => 'Avenida da França',
            'neighborhood' => 'Comércio',
            'city' => [
                'ddd' => 71,
                'ibge' => '2927408',
                'name' => 'Salvador'
            ],
            'state' => [
                'acronym' => 'BA'
            ]
        ];

        $storeInstance = $this->getStoreServiceInstance(true,$address);


        $CepClientService = $this->getCepClientServiceInstance(true,'40010000');
        // creating service
        $service = new AddressService($storeInstance,$logger,$CepClientService);

        // testing

        $newService = $this->makeMethodInvocable('TiendaNube\Checkout\Service\Shipping\AddressService','getAddressFromDatabase');
        $result = $newService->invokeArgs($service,['40010000']);

        // asserts
        $this->assertTrue($result);
    }

    public function testGetNonexistentAddressByZipcode()
    {

        // mocking logger
        $logger = $this->createMock(LoggerInterface::class);

        $storeInstance = $this->getStoreServiceInstance();


        $CepClientService = $this->getCepClientServiceInstance(true,'');

        $service = new AddressService($storeInstance,$logger, $CepClientService);

        // testing
        $result = $service->getAddressByZip('40010001');

        // asserts
        $this->assertEquals(false,$result);

    }

    public function testGetAddressByZipcodeWithPdoException()
    {
        // mocking pdo
        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willThrowException(new \PDOException('An error occurred'));

        // mocking logger
        $logger = $this->createMock(LoggerInterface::class);

        $storeInstance = $this->getStoreServiceInstance(true,new \PDOException('An error occurred'));


        $CepClientService = $this->getCepClientServiceInstance(true,'');

        $service = new AddressService($storeInstance,$logger, $CepClientService);

        // testing
        $result = $service->getAddressByZip('');

        // asserts
        $this->assertEquals(false,$result);
    }


    public function getCepClientServiceInstance(
        $withValidCep = false,
        $zip = ''
    ){

        if($withValidCep) {
            $CepClientService = new CepClientService();
            $CepClientService->setCepParameter($zip);
            $CepClientService->get();
            return $CepClientService;
        }
        $CepClientService = newCepClientService();

        return $CepClientService;
    }


    public function getStoreServiceInstance(
        $withValidAddress = false,
        $data = []
    ){

        $StoreServiceInstance = $this->createMock(StoreService::class);
        if($withValidAddress) {
            $StoreServiceInstance->method('isBetaTester')->willReturn(true);
            return $StoreServiceInstance;
        }
        $StoreServiceInstance->method('isBetaTester')->willReturn(false);
        return $StoreServiceInstance;
    }

    public function makeMethodInvocable($class,$method){

        $class = new \ReflectionClass($class);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method;
    }
}
