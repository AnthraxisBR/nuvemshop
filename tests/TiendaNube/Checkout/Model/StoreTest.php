<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/17/2018
 * Time: 10:54 PM
 */

use PHPUnit\Framework\TestCase;
use \TiendaNube\Checkout\Model\Store;

class StoreTest extends TestCase
{

    /**
     * Test find a valid zip code with PDO
     */
    public function testFindZipCode(){

        $storeData = [
            'id' => 1,
            'name' => 'Loja 1',
            'email' => 'loja@contato.com.br',
            'betaTester' => 1
        ];

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt->method('execute')->willReturn(true);
        $stmt->method('rowCount')->willReturn(1);
        $stmt->method('fetch')->willReturn($storeData);

        $prepare = $this->getMockBuilder(Store::class)
            ->disableOriginalConstructor()
            ->setMethods(['prepare'])
            ->getMock();
        $prepare->method('prepare')->willReturn($stmt);

        $result = $prepare->findZipCode('40010000');

        $this->assertEquals($storeData,$result->getArray());

    }

    /**
     * Test find zpi code with pdo exception
     */
    public function testFindZipCodeWithPdoException(){

        $this->expectException(PDOException::class);

        $prepare = $this->getMockBuilder(Store::class)
            ->disableOriginalConstructor()
            ->setMethods(['prepare'])
            ->getMock();

        $prepare->method('prepare')->willThrowException(new PDOException('An error ocurred'));

        $result = $prepare->findZipCode('40010000');

    }
}