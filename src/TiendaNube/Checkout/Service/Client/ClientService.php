<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 5:58 PM
 */

namespace TiendaNube\Checkout\Service\Client;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * ClientService class,
 * Class ClientService
 * @package TiendaNube\Checkout\Service\Client
 */
class ClientService extends Client
{

    /**
     * @var string
     */
    public $uri = '';

    /**
     * @var string
     */
    public $url = '';

    /**
     * @var array
     */
    public $options = [];

    /**
     * ClientService constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(){
        try {
            if(count($this->options) > 0){

                return $this->request('GET', $this->getFullUrl(), $this->options);
            }else{
                return $this->request('GET', $this->getFullUrl());
            }

        }catch (RequestException $ex){
            return false;
        }
    }

    /**
     * Set headers on option atribute array
     * @param array $headers
     */
    public function setOptionsHeaders(array $headers){
        $this->options['headers'] = [];

        foreach ($headers as $key => $value){
            if(is_array($value)){
                $value = implode(";",$value);
            }
            $this->options['headers'][$key] = $value;
        }
    }

    /**
     * Return full url
     * @return string
     */
    public function getFullUrl(){
        return $this->getUri() . $this->getUrn();
    }

    /**
     * Returns url
     * @return string
     */
    public function getUrn(){
        return $this->url;
    }

    /**
     * Returns uri
     * @return string
     */
    public function getUri(){
        return $this->uri;
    }

    /**
     * Define a URI, needs to be array
     * @param array $config
     */
    public function setServiceUri(array $config){
        $this->uri = $config['uri'];
    }

    /**
     * Set service URL
     * @param string $url
     */
    public function setServiceUrn(string $url){
        $this->url = ltrim($url,'/');
    }

}