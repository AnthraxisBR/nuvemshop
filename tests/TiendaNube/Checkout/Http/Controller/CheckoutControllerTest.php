<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Http\Controller;

use PHPUnit\Framework\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Psr7\Response;
use TiendaNube\Checkout\Http\Request\RequestStack;
use TiendaNube\Checkout\Http\Response\ResponseBuilder;
use TiendaNube\Checkout\Service\Shipping\AddressService;
use TiendaNube\Checkout\Service\Store\StoreService;
use TiendaNube\Checkout\Service\Client\CepClientService;

class CheckoutControllerTest extends TestCase
{

    /**
     * Teste the Controller with a valiz zip code
     */
    public function testGetAddressValid()
    {

        $address = [
            'altitude' => '7.0',
            'cep' => '40010000',
            'latitude' => '-12.967192',
            'longitude' => '-38.5101976',
            'address' => 'Avenida da França',
            'neighborhood' => 'Comércio',
            'city' => [
                'ddd' => 71,
                'ibge' => '2927408',
                'name' => 'Salvador'
            ],
            'state' => [
                'acronym' => 'BA'
            ]
        ];


        // getting controller instance
        $controller = $this->getControllerInstance();

        // mocking address service
        $addressService = $this->getAddressServiceInstance(1,$address);

        // test
        $result = $controller->getAddressAction('400100001',$addressService);

        // asserts
        $this->assertEquals(json_encode($address),$result->getBody()->getContents());
    }

    /**
     * Test Controller instance using a invalid zip code
     * @return null
     */
    public function testGetAddressInvalid()
    {
        // getting controller instance
        $controller = $this->getControllerInstance();

        // mocking address service
        $addressService = $this->getAddressServiceInstance();

        // test
        $result = $controller->getAddressAction('400100001',$addressService);

        // asserts
        $this->assertEquals(404,$result->getStatusCode());
        $this->assertEquals('{"error":"The requested zipcode was not found."}',$result->getBody()->getContents());
    }

    /**
     * Get a RequestStack mock object
     *
     * @param ServerRequestInterface|null $expectedRequest
     * @return MockObject
     */
    private function getRequestStackInstance(?ServerRequestInterface $expectedRequest = null)
    {
        $requestStack = $this->createMock(RequestStack::class);
        $expectedRequest = $expectedRequest ?: $this->createMock(ServerRequestInterface::class);
        $requestStack->method('getCurrentRequest')->willReturn($expectedRequest);

        return $requestStack;
    }

    /**
     * Get a ResponseBuilder mock object
     *
     * @param ResponseInterface|callable|null $expectedResponse
     * @return MockObject
     */
    private function getResponseBuilderInstance($expectedResponse = null)
    {
        $responseBuilder = $this->createMock(ResponseBuilder::class);

        if (is_null($expectedResponse)) {
            $expectedResponse = function ($body, $status, $headers) {
                $stream = $this->createMock(StreamInterface::class);
                $stream->method('getContents')->willReturn($body);

                $response = $this->createMock(ResponseInterface::class);
                $response->method('getBody')->willReturn($stream);
                $response->method('getStatusCode')->willReturn($status);
                $response->method('getHeaders')->willReturn($headers);

                return $response;
            };
        }

        if ($expectedResponse instanceof ResponseInterface) {
            $responseBuilder->method('buildResponse')->willReturn($expectedResponse);
        } else if (is_callable($expectedResponse)) {
            $responseBuilder->method('buildResponse')->willReturnCallback($expectedResponse);
        } else {
            throw new Exception(
                'The expectedResponse argument should be an instance (or mock) of ResponseInterface or callable.'
            );
        }

        return $responseBuilder;
    }


    /**
     * Get Logger instance
     * @return MockObject
     */
    private function getLoggerInstance() {


        $LoggerInstance = $this->createMock(LoggerInterface::class);
        $LoggerInstance->method('log')->willReturn(true);

        return $LoggerInstance;

    }


    /**
     * Get Address Service isntance
     * @param bool $withValidAddress
     * @return AddressService
     */
    private function getAddressServiceInstance(
        $withValidAddress = false,
        $data = []
    ) {


        $LoggerInstance = $this->getLoggerInstance();
        $storeInstance = $this->getStoreServiceInstance($withValidAddress,$data);

        $requestInstance = $this->createMock(Response::class);
        $requestInstance->method('getStatusCode')->willReturn(400);
        $requestInstance->method('getBody')->willReturn('');

        if(isset($data['cep'])){
            $cep  = $data['cep'];
        }else{
            $cep = '';
        }
        $CepClientService = $this->getCepClientServiceInstance(true,$cep);

        return new AddressService($storeInstance,$LoggerInstance,$CepClientService);
    }

    /**
     * Get an instance of the controller
     *
     * @param bool $withValidAddress
     * @param null|RequestStack $requestStack
     * @param null|ResponseBuilder $responseBuilder
     * @param null|AddressService $addressService
     * @return CheckoutController
     */
    private function getControllerInstance(
        $withValidAddress = false,
        ?RequestStack $requestStack = null,
        ?ResponseBuilder $responseBuilder = null/*,
        ?AddressService $addressService = null*/
    ) {
        // mocking units

        $container = $this->createMock(ContainerInterface::class);

        $requestStack = $requestStack ?: $this->getRequestStackInstance();

        $responseBuilder = $responseBuilder ?: $this->getResponseBuilderInstance();

        //$addressService = $addressService ?: $this->getAddressServiceInstance($withValidAddress);

        return new CheckoutController($container,$requestStack,$responseBuilder/*,$addressService*/);
    }

    /**
     * Get CepClient Service instance
     * @param bool $withValidCep
     * @param string $zip
     * @return CepClientService
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCepClientServiceInstance(
        $withValidCep = false,
        $zip = ''
    ){

        if($withValidCep) {
            $CepClientService = new CepClientService();
            $CepClientService->setCepParameter($zip);
            $CepClientService->get();
            return $CepClientService;
        }
        $CepClientService = newCepClientService();

        return $CepClientService;
    }

    /**
     * Get a Store Service instance
     * @param bool $withValidAddress
     * @param array $data
     * @return MockObject
     */
    public function getStoreServiceInstance(
        $withValidAddress = false
    ){

        $StoreServiceInstance = $this->createMock(StoreService::class);
        if($withValidAddress) {
            $StoreServiceInstance->method('isBetaTester')->willReturn(true);
            return $StoreServiceInstance;
        }
        $StoreServiceInstance->method('isBetaTester')->willReturn(false);
        return $StoreServiceInstance;
    }

}
