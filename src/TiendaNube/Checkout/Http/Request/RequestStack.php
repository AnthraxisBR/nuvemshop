<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 11:21 AM
 */

namespace TiendaNube\Checkout\Http\Request;


use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ServerRequestInterface;
use TiendaNube\Checkout\Contracts\Http\Request\RequestStack as RequestStackContract;

class RequestStack extends Request implements RequestStackContract
{
    public function getCurrentRequest(): ServerRequestInterface
    {
        return $this;
    }

}