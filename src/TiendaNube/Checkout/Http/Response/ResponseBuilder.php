<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/15/2018
 * Time: 12:50 PM
 */

namespace TiendaNube\Checkout\Http\Response;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use TiendaNube\Checkout\Contracts\Http\Response\ResponseBuilder as ResponseBuilderContract;

class ResponseBuilder extends Response implements ResponseBuilderContract
{

    /**
     * ResponseBuilder constructor.
     * @param int $status
     * @param array $headers
     * @param null $body
     * @param string $version
     */
    public function __construct(int $status = 200, array $headers = [], $body = null, string $version = '1.1')
    {
        parent::__construct($status, $headers, $body, $version, $this->getReasonPhrase($status));
    }

    /**
     * Calls self constructor again, and return itself class
     * @param mixed $body
     * @param int $status
     * @param array $headers
     * @return ResponseInterface
     */
    public function buildResponse($body, int $status = 200, array $headers = []): ResponseInterface
    {
        $this->__construct($body, $status , $headers );
        return $this;
    }



}