<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Contracts\Http\Request;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface RequestStack
 * @package TiendaNube\Checkout\Contracts\Http\Request
 */
interface RequestStack
{
    /**
     * Get the current request object
     *
     * @return ServerRequestInterface
     */
    public function getCurrentRequest():ServerRequestInterface;
}
