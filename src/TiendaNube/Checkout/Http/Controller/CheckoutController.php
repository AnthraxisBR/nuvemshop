<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Http\Controller;

use Psr\Http\Message\ResponseInterface;
use TiendaNube\Checkout\Service\Shipping\AddressService;

class CheckoutController extends AbstractController
{


    /**
     * Returns the address to be auto-fill the checkout form
     *
     * @Route /address/{zipcode}
     *
     * @param string $zipcode
     * @param AddressService $addressService
     * @return ResponseInterface
     */
    public function getAddressAction(string $zipcode, AddressService $addressService):ResponseInterface {

        // filtering and sanitizing input
        $rawZipcode = preg_replace("/[^\d]/","",$zipcode);

        // getting address by zipcode
        try {

            $address = $addressService->getAddressByZip($rawZipcode);
            // checking the result
            if ($address !== false) {
                return $this->json($address,200);
            }
            // returning the error when not found
            return $this->json(['error'=>'The requested zipcode was not found.'],404);
        }catch (\Exception $ex){
            return $this->json([],500);
        }


    }
}
