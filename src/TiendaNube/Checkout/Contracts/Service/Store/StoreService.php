<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Contracts\Service\Store;

use TiendaNube\Checkout\Model\Store;

/**
 * Interface StoreService
 *
 * @package TiendaNube\Checkout\Service\Store
 */
interface StoreService
{
    /**
     * Get the current store instance
     *
     * @return Store
     */
    public function getCurrentStore():Store;

    /**
     * @return bool
     */
    public function isBetaTester(): bool ;
}
