<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Model;

/**
 * Class Store
 *
 * @package TiendaNube\Checkout\Model
 */
class Store extends \PDO
{

    /**
     * The store's id
     *
     * @var int
     */
    private $id = 0;

    /**
     * The store's name
     *
     * @var string
     */
    private $name;

    /**
     * The store's owner e-mail address
     *
     * @var string
     */
    private $email;

    /**
     * The store's beta tester status
     *
     * @var bool
     */
    private $betaTester = false;

    /**
     * Use itself pdo heritage to find address in database
     * @param $zip
     * @return $this|bool
     */
    public function findZipCode($zip){
        // getting the address from database
        $stmt = $this->prepare('SELECT * FROM `addresses` WHERE `zipcode` = ?');
        $stmt->execute([$zip]);

        // checking if the address exists
        if ($stmt->rowCount() > 0) {

            $data = $stmt->fetch(\PDO::FETCH_ASSOC);

            $this->id = $data['id'];
            $this->name = $data['name'];
            $this->email = $data['email'];
            $this->betaTester = $data['betaTester'];

            return $this;
        }
        return false;
    }

    /**
     * Returns store array
     * @return array
     */
    public function getArray(){
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'betaTester' => $this->betaTester
        ];
    }

    /**
     * Check if model has a valid store
     * @return bool
     */
    public function hasStore(){
        return $this->id !== 0;
    }

    /**
     * Get the current store ID
     *
     * @return int
     */
    public function getId():int {
        return $this->id;
    }

    /**
     * Set the store's name
     *
     * @param string $name
     */
    public function setName(string $name):void {
        $this->name = $name;
    }

    /**
     * Get the current store name
     *
     * @return string
     */
    public function getName():string {
        return $this->name;
    }

    /**
     * Sets the store's e-mail address
     *
     * @param string $email
     */
    public function setEmail(string $email):void {
        $this->email = $email;
    }

    /**
     * Get the current store e-mail address
     * @return string
     */
    public function getEmail():string {
        return $this->email;
    }

    /**
     * Check if the current store is a beta tester
     *
     * @return bool
     */
    public function isBetaTester():bool {
        return $this->betaTester;
    }

    /**
     * Enables store beta testing
     */
    public function enableBetaTesting():void {
        $this->betaTester = true;
    }

    /**
     * Disables store beta testing
     */
    public function disableBetaTesting():void {
        $this->betaTester = false;
    }
}
