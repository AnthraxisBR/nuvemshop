<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 6:40 PM
 */

namespace TiendaNube\Checkout\Exception;

use \GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CepClientServiceException extends ClientException
{
    public function __construct($message, RequestInterface $request, ResponseInterface $response = null, \Exception $previous = null, array $handlerContext = [])
    {
        parent::__construct($message, $request, $response, $previous, $handlerContext);
    }
}