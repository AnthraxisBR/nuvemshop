<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 5:21 PM
 */

namespace TiendaNube\Checkout\Contracts\Service\Client;

/**
 * Interface CepAbertoClientService
 * @package TiendaNube\Checkout\Contracts\Service\Client
 */
interface CepAbertoClientService
{
    /**
     * Boot CepAberto basic configurations
     * @param array $config
     * @return mixed
     */
    public function boot(array $config);

}