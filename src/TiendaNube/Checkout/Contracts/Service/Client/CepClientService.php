<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/17/2018
 * Time: 10:36 PM
 */

namespace TiendaNube\Checkout\Contracts\Service\Client;


interface CepClientService
{
    public function setCepParameter(string $zip);

    public function make(array $config = []);
}