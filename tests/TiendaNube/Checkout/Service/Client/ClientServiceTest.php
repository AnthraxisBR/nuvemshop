<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/17/2018
 * Time: 9:18 PM
 */

use PHPUnit\Framework\TestCase;
use TiendaNube\Checkout\Service\Client\ClientService;


class ClientServiceTest extends TestCase
{

    /**
     * Test ClientService with a valid address
     */
    public function testGetExistentAddress(){

        $clientSevice = new ClientService();
        $clientSevice->setServiceUri(['uri' => 'https://google.com/']);
        $clientSevice->setServiceUrn('doodles/');
        $this->assertEquals(200,$clientSevice->get()->getStatusCode());

    }

    /**
     * Test ClientService for a non existent address
     */
    public function testGetNonExistentAddress(){

        $clientSevice = new ClientService();
        $clientSevice->setServiceUri(['uri' => 'https://donttouch.me/']);
        $clientSevice->setServiceUrn('not');
        $this->assertFalse($clientSevice->get());

    }

}