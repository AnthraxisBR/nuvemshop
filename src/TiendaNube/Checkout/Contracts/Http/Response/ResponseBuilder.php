<?php
declare(strict_types=1);

namespace TiendaNube\Checkout\Contracts\Http\Response;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ResponseBuilder
 * @package TiendaNube\Checkout\Contracts\Http\Response
 */
interface ResponseBuilder
{
    /**
     * Factory a ResponseBuilder object
     *
     * @param mixed $body
     * @param int $status
     * @param array $headers
     * @return ResponseBuilder
     */
    public function buildResponse($body, int $status = 200, array $headers = []): ResponseInterface;

}
