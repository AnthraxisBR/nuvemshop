<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/17/2018
 * Time: 10:44 PM
 */

namespace TiendaNube\Checkout\Contracts\Service\Client;

/**
 * Interface ClientService
 * @package TiendaNube\Checkout\Contracts\Service\Client
 */
interface ClientService
{
    /**
     * @return bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get();

    /**
     * Set headers on option atribute array
     * @param array $headers
     */
    public function setOptionsHeaders(array $headers);

    /**
     * Return full url
     * @return string
     */
    public function getFullUrl();

    /**
     * Returns uri
     * @return string
     */
    public function getUri();

    /**
     * Returns url
     * @return string
     */
    public function getUrn();

    /**
     * Define a URI, needs to be array
     * @param array $config
     */
    public function setServiceUri(array $config);

    /**
     * Set service URL
     * @param string $url
     */
    public function setServiceUrn(string $url);
}