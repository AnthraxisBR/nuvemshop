<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 6:39 PM
 */

namespace TiendaNube\Checkout\Exception;


use Throwable;

class StoreException extends \PDOException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}