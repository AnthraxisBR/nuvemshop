<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 5:43 PM
 */

namespace TiendaNube\Checkout\Service\Client;

use TiendaNube\Checkout\Contracts\Service\Client\CepClientService as CepClientServiceContract;

/**
 * Class to search zip code directly in CepAberto api search address
 * Usage:
 *
 *   $CepClientService = new CepClientService();
 *   $CepClientService->setCepParameter('400100001');
 *   CepClientService->get();
 *
 * Class CepClientService
 * @package TiendaNube\Checkout\Service\Client
 */
class CepClientService extends CepAbertoClientService implements CepClientServiceContract
{

    /**
     * The client uri
     * @var
     */
    public $uri;

    /**
     * Set default URL for replace, and call parent constructor
     * CepClientService constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->setServiceUrn('?cep={var}');
        parent::__construct($config);
    }

    /**
     * Replace actual url paremeter for zip code
     * @param string $zip
     */
    public function setCepParameter(string $zip){
        $this->url = str_replace('{var}',$zip,$this->url);
    }

    /**
     * Same of __constructor
     * Set default URL for replace, and call parent constructor
     * @param array $config
     */
    public function make(array $config = []){
        $this->setServiceUrn('?cep={var}');

        parent::__construct($config);
    }
}