<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/15/2018
 * Time: 7:58 PM
 */

namespace TiendaNube\Checkout\Service\Store;

use TiendaNube\Checkout\Contracts\Service\Store\StoreService as StoreContract;
use TiendaNube\Checkout\Model\Store;

class StoreService implements StoreContract
{

    /**
     * The database connection link
     *
     * @var \TiendaNube\Checkout\Model\Store
     */
    public $model;

    /**
     * StoreService constructor.
     * Start a new store model instance if no one model has been informed
     * @param Store|null $model
     */
    public function __construct(Store $model = null)
    {
        if(is_null($model)){
            $this->model = new Store();
        }else{
            $this->model = $model;
        }
    }

    /**
     * Returns current store in the model
     * @return Store
     */
    public function getCurrentStore():Store
    {
        return $this->model;
    }


    /**
     * Check if actual model is Beta Tester
     * @return bool
     */
    public function isBetaTester(): bool{
        return parent::isBetaTester();
    }
}