<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 12:29 PM
 */

namespace TiendaNube\Checkout\Contracts\Service\Shipping;

/**
 * Interface AddressService
 * @package TiendaNube\Checkout\Contracts\Service\Shipping
 */
interface AddressService
{
    /**
     * Get an address by its zipcode (CEP)
     *
     * The expected return format is an array like:
     * [
     *      'altitude'=>'7.0',
     *      'cep'=>'40010000',
     *      'latitude'=>'-12.967192',
     *      'longitude'=>'-38.5101976',
     *      'address'=>'Avenida da França',
     *      'neighborhood'=>'Comércio',
     *      'city'=>[
     *          'ddd'=>71,
     *          'ibge'=>'2927408',
     *          'name'=>'Salvador'
     *      ],
     *      'state'=>[
     *          'acronym'=>'BA'
     *      ]
     * ];
     *
     * or false when not found.
     *
     * @param string $zip
     * @return bool|mixed
     */
    function getAddressByZip(string $zip);

    /**
     * Get full address data from external API (CepAberto http://www.cepaberto.com/api_key )
     *
     * Will return all data from the external API, or false if an error occurred or nothing has founded
     *
     * @param string $zip
     * @return array|mixed
     */
    function getAddressZipData(string $zip);

    /**
     * Get an address by its zipcode (CEP) from Database
     *
     * The expected return is boolean of search status, the result is store on $this->store->model object
     * @param string $zip
     * @return bool
     */
    function getAddressFromDatabase(string $zip);

    /**
     * @param array $data
     * @return array
     */
    function formatAddressZipDataResponse($data);
}