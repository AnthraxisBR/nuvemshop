<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 5:20 PM
 */

namespace TiendaNube\Checkout\Service\Client;

use TiendaNube\Checkout\Contracts\Service\Client\CepAbertoClientService as CepAbertoClientServiceContract;

/**
 * CepAberto Client class, use it to connect direct to CepAberto api address (http://www.cepaberto.com/api/v3/cep)
 *
 * Usage:
 *  $CepAbertoClientService = new CepAbertoClientService();
 *  $CepAbertoClientService->setServiceUrn('400100001')
 *  $CepAbertoClientService->get()
 *
 * Class CepAbertoClientService
 * @package TiendaNube\Checkout\Service\Client
 */
class CepAbertoClientService extends ClientService implements CepAbertoClientServiceContract
{

    /**
     * CepAberto configs
     * @var array
     */
    private $CepAbertoConfig= [];

    /**
     * Call boot method e parent constructor
     * CepAbertoClientService constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {

        $this->boot($config);

        parent::__construct($config);
    }

    /**
     * Include Boot file
     * Merge parameter configurations and CepAberto configurations
     * Set service Uri
     * Set Service Headers
     * @param array $config
     */
    public function boot(array $config){
        $this->CepAbertoConfig = include __DIR__ . '\..\..\config\CepAberto.php';

        $config = array_merge($config,$this->CepAbertoConfig);

        $this->setServiceUri($config);

        $this->setOptionsHeaders($config['headers']);
    }

}