<?php

declare(strict_types=1);

namespace TiendaNube\Checkout\Http\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TiendaNube\Checkout\Http\Request\RequestStack;
use TiendaNube\Checkout\Http\Response\ResponseBuilder;
use TiendaNube\Checkout\Service\Shipping\AddressService;

/**
 * Class AbstractController
 *
 * @package TiendaNube\Checkout\Controller
 */
abstract class AbstractController
{
    /**
     * DependencyInjection container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * The RequestStack object
     *
     * @var RequestStack
     */
    private $requestStack;

    /**
     * The ResponseBuilder
     *
     * @var ResponseBuilder
     */
    private $responseBuilder;

    /**
     * The AddressService
     *
     * @var AddressService
     */
    public $addressService;

    /**
     * AbstractController constructor.
     *
     * @param ContainerInterface $container
     * @param RequestStack $requestStack
     * @param ResponseBuilder $responseBuilder
     */
    public function __construct(
        ContainerInterface $container,
        RequestStack $requestStack
    ) {
        $this->container = $container;
        $this->requestStack = $requestStack;
    }

    /**
     * Get the current request
     *
     * @return ServerRequestInterface
     */
    protected function getCurrentRequest():ServerRequestInterface {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * Get the DependencyInjection container
     *
     * @return ContainerInterface
     */
    protected function getContainer():ContainerInterface {
        return $this->container;
    }

    /**
     * Get a JsonResponse object
     *
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return ResponseInterface
     */
    protected function json(array $data, int $status = 200, array $headers = []):ResponseInterface {
        return new ResponseBuilder($status, $headers, json_encode($data), '1.1');
    }
}
