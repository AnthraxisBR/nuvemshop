<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 12/16/2018
 * Time: 5:33 PM
 */

/**
 * Contains configuration data for CapAberto services
 */
return [
    'uri'=>'http://www.cepaberto.com/api/v3/cep',
    'headers' => [
        'Authorization' => ' Token token=cd3085b6a8929334acf3f4c5b3a8a584'
    ]
];
